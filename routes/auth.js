
const express  = require('express');
const passport = require('passport');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require('../models');

const router = express.Router();

module.exports = router;

// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", (req, res) => {
  // Form validation
  // const { errors, isValid } = validateRegisterInput(req.body);

  // // Check validation
  // if (!isValid) {
  //   return res.status(400).json(errors);
  // }

  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        about_me: req.body.about_me
      });

      // Hash password before saving in database
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then((user) => {
              const payload = {
                id: user.id,
                name: user.username,
                about_me: user.about_me
              };
              jwt.sign(
                payload,
                process.env.SESSION_SECRET,
                {
                  expiresIn: 31556926, // 1 year in seconds
                },
                (err, token) => {
                  res.json({
                    user: {
                      success: true,
                      token: "Bearer " + token,
                    }
                  });
                }
              );
            })
            .catch((err) => console.log(err));
        });
      });
    }
  });
});

router.post("/login", (req, res) => {
  // Form validation
  // const { errors, isValid } = validateLoginInput(req.body);

  // Check validation
  // if (!isValid) {
  //   return res.status(400).json(errors);
  // }

  const email = req.body.email;
  const password = req.body.password;

  // Find user by email
  User.findOne({ email }).then((user) => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }

    // Check password
    bcrypt.compare(password, user.password).then((isMatch) => {
      if (isMatch) {
        // User matched
        // Create JWT Payload
        const payload = {
          id: user.id,
          name: user.username,
          about_me: user.about_me
        };

        // Sign token
        jwt.sign(
          payload,
          process.env.SESSION_SECRET,
          {
            expiresIn: 31556926, // 1 year in seconds
          },
          (err, token) => {
            res.json({
              user: {
                success: true,
                token: "Bearer " + token,
              }
            });
          }
        );
      } else {
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});

// router.post('/register', (req, res) => {
//   if (!req || !req.body || !req.body.username || !req.body.password) {
//     res.status(400).send({ message: 'Username and Password required' });
//   }

//   req.body.username_case = req.body.username;
//   req.body.username = req.body.username.toLowerCase();

//   const { username } = req.body;
//   const newUser = User(req.body);

//   User.find({ username }, (err, users) => {
//     if (err) {
//       res.status(400).send({ message: 'Create user failed', err });
//     }
//     if (users[0]) {
//       res.status(400).send({ message: 'Username exists' });
//     }

//     newUser.hashPassword().then(() => {
//       newUser.save((err, savedUser) => {
//         if (err || !savedUser) {
//           res.status(400).send({ message: 'Create user failed', err });
//         } else {
//           res.send({ message: 'User created successfully', user: savedUser.hidePassword() });
//         }
//       });
//     });

//   });
// });

// router.post('/login', (req, res, next) => {
//   req.body.username = req.body.username.toLowerCase();

//   passport.authenticate('local', (err, user, info) => {
//     if (err) {
//       return next(err);
//     }
//     if (!user) {
//       return res.status(401).send(info);
//     }

//     req.login(user, err => {
//       debugger
//       console.log(user);
//       if (err) {
//         res.status(401).send({ message: 'Login failed', err });
//       }
//       res.send({ message: 'Logged in successfully', user: user.hidePassword() });
//     });

//   })(req, res, next);
// });

router.post('/logout', (req, res) => {
  req.session.destroy(err => {
    if (err) {
      res.status(400).send({ message: 'Logout failed', err });
    }
    req.sessionID = null;
    req.logout();
    res.send({ message: 'Logged out successfully' });
  });
});

router.post('/checkusername', (req, res) => {
  const username = req.body.username.toLowerCase();

  User.find({ username }, (err, users) => {
    if (err) {
      res.status(400).send({ message: 'Check username failed', err, username });
    }
    if (users && users[0]) {
      res.send({ available: false, message: 'Username exists', username });
    } else {
      res.send({ available: true, message: 'Username available', username });
    }
  });
});