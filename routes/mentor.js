const express  = require('express');
const AWS = require('aws-sdk');

const mongoose = require('mongoose');
const { User, Meeting } = require('../models');
const passport = require('passport');
const { v4: uuid } = require('uuid');

const router = express.Router();

module.exports = router;

const excludeParams = {"password": 0}
const region = 'us-east-1'


AWS.config.credentials = new AWS.Credentials(process.env.AWS_SECRET_KEY, process.env.AWS_SECRET_TOKEN, null);
const chime = new AWS.Chime({ region: region })
chime.endpoint = new AWS.Endpoint(
  'https://service.chime.aws.amazon.com/console'
)


router.get('/search', (req, res) => {
  let page = req.query.page || 1;
  let offset = 10;
  let skip = (page * offset) - offset;
  let q = req.query.q;
  let param = q ? {$text: {$search: req.query.q}} : {}

  User.find(param).skip(skip).limit(offset).select(excludeParams)
  .exec(function(err, user) {
    if (err) {
      res.json(err);
    } else {
      res.json({"mentors": user} );
    }
  });
});

router.get('/join', async (req, res) => {
  try {
    let meeting = await Meeting.findOne({"external_meeting_id": req.query.meeting_id})
    if (meeting) {
      let attendee = await chime
      .createAttendee({
          MeetingId:req.query.meeting_id,
          ExternalUserId: req.query.attendee_id,
      })
      .promise()
      console.log(meeting)
      res.json({
        attendee: attendee,
        meetingResponse: meeting.service_info
      })
    } else {
      res.json({error: "invalid Meeting"})
    }

  } catch (err) {
    console.error(err)
    res.json(err)
  }
});

router.post('/:mentor_id/start', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const client_token = uuid();
    let chime_meeting = await chime
    .createMeeting({
      ExternalMeetingId: uuid(),
      ClientRequestToken: client_token,
      MediaRegion: region,
    })
    .promise()


    let response = {meetingResponse: chime_meeting}
    let meeting = await Meeting.create({
      client_request_token: client_token,
      service_info: chime_meeting,
      external_meeting_id: chime_meeting.Meeting.MeetingId,
    })
    console.log(meeting)
    console.log(response)
    res.json(response)
  } catch (err) {
    console.error(err)
    res.json(err)
  }
});

router.get('/:mentor_id', (req, res) => {
  const mentor_id = req.params.mentor_id
  User.findOne({"_id": mentor_id}, excludeParams,  (err, mentor) => {
    if (err) {
      res.json(err);
    } else {
      res.json({mentor: mentor});
    }
  })
});

