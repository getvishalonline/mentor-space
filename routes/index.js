const express = require('express');
const path = require('path');

const auth         = require('./auth');
const user         = require('./user');
const mentor         = require('./mentor');

const router = express.Router();
router.use('/api/auth', auth);
router.use('/api/user', user);
router.use('/api/mentor', mentor);
router.get('/*', (req, res) => {
  res.send('Heartbeat OK!');
});

module.exports = router;