const express      = require('express');
const path         = require('path');
const bodyParser   = require('body-parser');
const cors = require('cors')

require('dotenv').config({path: __dirname+'/.env'});
require('./config');
require('./models');

const routes          = require('./routes/index');
const configPassport  = require('./passport/passport-config');

// const assetFolder  = path.resolve(__dirname, '../dist/');
const port         = process.env.PORT;
const app          = express();

// Bodyparser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors());

configPassport(app, express);

app.use('/', routes);

app.listen(port, () => console.log(`Server is listening on port ${port}`));