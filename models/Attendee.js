const mongoose = require('mongoose');

const { Schema } = mongoose;

const attendeeSchema = new Schema({
  attendee_id: String,
  token: String,
  user: { type: Schema.Types.ObjectId, ref: 'User' },
});

const Attendee = mongoose.model('Attendee', attendeeSchema);

module.exports = attendeeSchema;