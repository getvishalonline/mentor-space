const mongoose = require('mongoose');

const attendeeSchema = require('./Attendee');

const { Schema } = mongoose;

const meetingSchema = new Schema({
  attendees: [attendeeSchema],
  client_request_token: String,
  service_info: Object,
  external_meeting_id: String
});

const Meeting = mongoose.model('Meeting', meetingSchema);

module.exports = Meeting;