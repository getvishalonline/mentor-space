const passport    = require('passport');
const session     = require('express-session');
const MongoStore  = require('connect-mongo');
const uuid        = require('uuid');
const mongoose    = require('mongoose');

const Strategies  = require('./strategies.js');
const { User }    = require('../models');

module.exports = app => {
  const sessionConfig = {
    store: new MongoStore({
      mongoUrl: process.env.DATABASE_URL,
      mongooseConnection: mongoose.connection,
      collection: 'sessions',
    }),
    genid: () => uuid.v4(),
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  };

  app.use(session(sessionConfig));
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => done(null, user.id));

  passport.deserializeUser((id, done) =>
    User.findById({ _id: id })
      .then(user => done(null, user))
      .catch(err => console.warn(`err at deserialize: ${err}`)));

  passport.use(Strategies.jwt);
};