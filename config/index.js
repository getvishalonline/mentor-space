const Environment = require('./environment');
const Datastore = require('./datastore');

module.exports = {
  Datastore,
  Environment
};