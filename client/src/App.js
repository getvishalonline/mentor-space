
import './App.css';

import { Button } from 'rsuite';

function App() {
  return (
    <div className="App">
      <Button appearance="primary"> Hello world </Button>
    </div>
  );
}

export default App;
