import request from 'superagent';
import { handleSuccess, handleError } from '../utils/api';

const baseUrl = `${process.env.REACT_APP_API_URL}/api/user`

export const getUser = () =>
  request.get(`${baseUrl}`)
    .then(handleSuccess)
    .catch(handleError);

export const postCheckUsername = username =>
  request.post(`${baseUrl}/checkusername`)
    .send({ username })
    .then(handleSuccess)
    .catch(handleError);