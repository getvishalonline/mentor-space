import request from 'superagent';
import { handleSuccess, handleError } from '../utils/api';

const baseUrl = `${process.env.REACT_APP_API_URL}/api/mentor`

export const searchMentor = query => {
  console.log(process.env.REACT_APP_API_URL)
  return request.get(`${baseUrl}/search`)
    .query(query)
    .then(handleSuccess)
    .catch(handleError);
}

export const getMentor = query => {
  return request.get(`${baseUrl}/${query.mentor_id}`)
    .query({})
    .then(handleSuccess)
    .catch(handleError);
}

export const startMeeting = query => {
  return request.post(`${baseUrl}/${query.attendee_id}/start`)
  .send({meeting_id: query.meeting_id})
  .set('Authorization', localStorage['mnt_session'])
  .then(handleSuccess)
  .catch(handleError);
}

export const joinMeeting = query => {
  console.log(query)
  return request.get(`${baseUrl}/join`)
  .query({meeting_id: query.meeting_id, attendee_id: query.attendee_id})
  .then(handleSuccess)
  .catch(handleError);
}
