import request from 'superagent';
import { handleSuccess, handleError } from '../utils/api';

const baseUrl = `${process.env.REACT_APP_API_URL}/api/auth`

export const postRegister = user =>
  request.post(`${baseUrl}/register`)
    .send(user)
    .then(handleSuccess)
    .catch(handleError);

export const postLogin = user =>
  request.post(`${baseUrl}/login`)
    .send(user)
    .then(handleSuccess)
    .catch(handleError);