import { push } from 'connected-react-router';
import { snakeToCamelCase } from 'json-style-converter/es5';
import jwt_decode from "jwt-decode";

import { postRegister, postLogin } from '../../api/auth';
import { login } from '../actions/user';

import { dispatchError } from '../../utils/api';

export const attemptLogin = user => dispatch =>
  postLogin(user)
    .then(data => {
      localStorage.setItem("mnt_session", data.user.token)
      dispatch(login(snakeToCamelCase(jwt_decode(data.user.token))));

      dispatch(push('/home'));
      return data;
    })
    .catch(dispatchError(dispatch));

export const attemptRegister = newUser => dispatch =>
  postRegister(newUser).then(data => {
      localStorage.setItem("mnt_session", data.user.token)
      dispatch(login(snakeToCamelCase(jwt_decode(data.user.token))));

      return data;
    })
    .then(() => dispatch(push('/home')))
    .catch(dispatchError(dispatch));

export const attemptLogout = () => dispatch => {
  localStorage.removeItem('mnt_session');
  dispatch(push('/login'));
}
  