import { snakeToCamelCase } from 'json-style-converter/es5';
import jwt_decode from "jwt-decode";

import { updateUser } from '../actions/user';

export const attemptGetUser = () => dispatch => {
   // Check for token to keep user logged in
   if (localStorage.mnt_session) {
    // Set auth token header auth
    const token = localStorage.mnt_session;

    // Decode token and get user info and exp
    const decoded = jwt_decode(token);

    dispatch(updateUser(snakeToCamelCase(decoded)));

    // Check for expired token
    const currentTime = Date.now() / 1000; // to get in milliseconds

    if (decoded.exp < currentTime) {
      // Logout user
      // store.dispatch(logoutUser());
      localStorage.mnt_session = null;

      // Redirect to login
      window.location.href = "./login";
    }
  }
}
