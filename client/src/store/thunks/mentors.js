import { mentorList } from '../actions/mentors';

import { dispatchError } from '../../utils/api';
import { searchMentor } from '../../api/mentors';

export const searchMentors = query => dispatch => {
  return searchMentor(query)
    .then(data => {
      dispatch(mentorList(data.mentors))
      return data;
    })
    .catch(dispatchError(dispatch));
}