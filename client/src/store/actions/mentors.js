export const MENTOR_LIST = 'MENTOR_LIST';

export function mentorList(mentors) {
  return {
    type: MENTOR_LIST,
    mentors
  }
}