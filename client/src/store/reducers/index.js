import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import user from './user';
import mentors from './mentors';


const createRootReducer = history => combineReducers({
  router: connectRouter(history),
  user,
  mentors
});

export default createRootReducer;