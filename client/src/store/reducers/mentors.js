import update from 'immutability-helper';
import { MENTOR_LIST } from '../actions/mentors';

export default function mentors(state = {}, action) {
  switch (action.type) {
    case MENTOR_LIST:
      return update(state, { $set: action.mentors });
    default:
      return state;
  }
}