
import React from 'react';
import { Row, Col, Panel} from 'rsuite';

export function MentorCards(props) {
  let { mentors } = props;
  if (mentors) {
    const listItems = (mentors || []).map((mentor) =>
      <Col xs={24}  key={mentor["_id"]}>
        <Panel bordered style={{marginTop: '10px'}}>
          <a href={`/mentor/${mentor["_id"]}`} style={{fontSize: '16px', color: '#000'}}>{mentor.username}</a>
          <p>{mentor.about_me}</p>
        </Panel>
      </Col>
    );
    return (
      <Row>{listItems}</Row>
    );
  } else {
    return(<></>)
  }
}