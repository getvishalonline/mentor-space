import React from 'react';
import { useDispatch } from 'react-redux';
import { attemptLogout } from '../../store/thunks/auth';
import { Button } from 'rsuite';

export function LogoutButton(props) {
  const dispatch = useDispatch();
  
  const logout = () =>
    dispatch(attemptLogout());

  return (
    <Button onClick={logout} { ...props }>Logout</Button>
  );
}
