import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { push } from 'connected-react-router';
import * as R from 'ramda';

import { getMentor } from '../../api/mentors';

import { Panel } from 'rsuite';
import Navigation from '../application/Navigation';


export default function ProfilePage({ location }) {
  const dispatch = useDispatch();
  const { user } = useSelector(R.pick(['user']));
  const { mentor_id } = useParams();
  const [loading, setLoading] = useState(true);
  const [ mentor, setMentor ] = useState(null);

  useEffect(() => {
    if (R.isEmpty(user)) {
      dispatch(push('/login'));
    } else {

      getMentor({mentor_id: mentor_id})
      .then( data => {
        setMentor(data.mentor);
        setLoading(false);
      });
    }
  }, []);

  function ProfileSection() {
    if (mentor) {
      return (
        <Panel bordered>
          <a href={`/mentor/${mentor["_id"]}`}>{mentor.username}</a>
          <p>{mentor.about_me}</p>
          <p><a href={`/mentor/${mentor["_id"]}/request`}>Request</a></p>
        </Panel>
      );
    } else {
      return(<></>)
    }
  
  }

  return(
    <>
      <Navigation />
      <div className="profile-page page">
        {!loading ? <ProfileSection /> : ''}
      </div>
    </>
  )
}


ProfilePage.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired
};