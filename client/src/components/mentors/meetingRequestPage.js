import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { push } from 'connected-react-router';
import * as Chime from 'amazon-chime-sdk-js';
import * as R from 'ramda';

import { getMentor, startMeeting, joinMeeting } from '../../api/mentors';

import { Container, Content, Schema, FlexboxGrid, Panel,
  Form, FormGroup, ControlLabel, FormControl, ButtonToolbar, Button, Col } from 'rsuite';

const { StringType } = Schema.Types;
const model = Schema.Model({
  username: StringType().isRequired('This field is required.'),
  email: StringType()
    .isEmail('Please enter a valid email address.')
    .isRequired('This field is required.'),
  about_me: StringType().isRequired('This field cannot be empty')
});
  

export default function MeetingRequestPage({ location }) {
  const dispatch = useDispatch();
  let form = null;
  const [formValue, setFormValue] = useState({
    username: '',
    password: '',
  })
  const [formError, setFormError] = useState({})
  const { user } = useSelector(R.pick(['user']));
  const { mentor_id } = useParams();
  const [loading, setLoading] = useState(true);
  const [ mentor, setMentor ] = useState(null);

  const [meetingId, setMeetingId] = useState()
  const [meetingResponse, setMeetingResponse] = useState()
  const [attendeeResponse, setAttendeeResponse] = useState()
  const [meetingSession, setMeetingSession] = useState()
  let  meeting_id, attendee_id; 
  const [callCreated, setCallCreated] = useState(false)
  const videoElement = useRef()
  const meetingIdElem = useRef()

  useEffect(() => {
    if (R.isEmpty(user)) {
      dispatch(push('/login'));
    } else {

      getMentor({mentor_id: mentor_id})
      .then( data => {
        setMentor(data.mentor);
        setLoading(false);
      });
    }
  }, []);

  const startCall = () => { 
    const urlSearchParams = new URLSearchParams(location.search);
    const qparams = Object.fromEntries(urlSearchParams.entries());
    let attendee_id = qparams.attendee_id
    startMeeting({attendee_id: attendee_id})
      .then((data) => {
        setMeetingResponse(data.meetingResponse)
        meetingIdElem.current.value = data.meetingResponse.Meeting.MeetingId
        setCallCreated(true)
    });
  }

  const joinVideoCall = async () => {
    const urlSearchParams = new URLSearchParams(location.search);
    const qparams = Object.fromEntries(urlSearchParams.entries());
    meeting_id = qparams.meeting_id ? qparams.meeting_id : meetingResponse.Meeting.MeetingId
    let attendee, meeting;
    await joinMeeting({attendee_id: mentor_id, meeting_id: meeting_id})
      .then((data) => {
        setAttendeeResponse(data.attendee)
        attendee = data.attendee
        meeting = data.meetingResponse
    });
    const logger = new Chime.ConsoleLogger('ChimeMeetingLogs', Chime.LogLevel.INFO);
    const deviceController = new Chime.DefaultDeviceController(logger);
    const configuration = new Chime.MeetingSessionConfiguration(meeting, attendee);
    const meetingSession = new Chime.DefaultMeetingSession(configuration, logger, deviceController);
    const observer = {
      audioVideoDidStart: () => {
        meetingSession.audioVideo.startLocalVideoTile();
      },
      videoTileDidUpdate: tileState => {
        if (!tileState.boundAttendeeId || tileState.localTile || tileState.isContent) {
          return;
        }
        
        meetingSession.audioVideo.bindVideoElement(tileState.tileId, videoElement.current);
      },
      audioVideoDidStop: sessionStatus => {
        // See the "Stopping a session" section for details.
        console.log('Stopped with a session status code: ', sessionStatus.statusCode());
      },
    }

    meetingSession.audioVideo.addObserver(observer);
    const firstVideoDeviceId = (await meetingSession.audioVideo.listVideoInputDevices())[0].deviceId;
    await meetingSession.audioVideo.chooseVideoInputDevice(firstVideoDeviceId);
    meetingSession.audioVideo.start();
    setMeetingSession(meetingSession);
  }

  const stopCall = () => {
    meetingSession.audioVideo.stop();
  }


  return(
    <div className="booking-page page">
      <Container>
        <Content>
          <video ref={videoElement}></video>
          <FlexboxGrid justify="center" align="middle">
            <FlexboxGrid.Item componentClass={Col} colspan={12} xs={24}>
              <Panel header={<h3>Meeting</h3>} bordered>
                <Form 
                model={model}
                ref={ (ref) => (form = ref)}
                onCheck={formError => { setFormError({ formError }); }}
                onChange={ formValue => setFormValue(formValue)}
                formValue={formValue}
                fluid>
                  <FormGroup>
                    <ControlLabel>Meeting Id</ControlLabel>
                    <FormControl name="meeting_id" ref={meetingIdElem}/>
                  </FormGroup>
                  <FormGroup>
                    <ButtonToolbar>
                      <Button appearance="primary" onClick={ startCall }>Start</Button>
                      <Button appearance="primary" disabled={!callCreated} onClick={ joinVideoCall }>Join</Button>
                      <Button appearance="primary" disabled={!callCreated} onClick={ stopCall }>Stop</Button>
                    </ButtonToolbar>
                  </FormGroup>
                </Form>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
      </Container>
    </div>
  )
}


MeetingRequestPage.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired
};