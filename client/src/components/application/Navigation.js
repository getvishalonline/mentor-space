import React  from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import * as R from 'ramda';
import '../../styles/Navigation.css';

import {Navbar, Nav } from 'rsuite';
import { LogoutButton } from '../molecules/LogoutButton';
import ReactNotification from 'react-notifications-component';

export default function Navigation({ pathname }) {
  const { user } = useSelector(R.pick(['user']));

  return user ? (<>
  <Navbar>
    <Navbar.Header>
      <a href="/" className="navbar-brand logo">MentorSpace</a>
    </Navbar.Header>
    <Navbar.Body>
      <Nav>
      </Nav>
      <Nav pullRight>
        <li><LogoutButton className="nav-action" /></li>
      </Nav>
    </Navbar.Body>
  </Navbar>
   <ReactNotification />
   </>) : (<><ReactNotification /></>);
}

Navigation.propTypes = {
  pathname: PropTypes.string.isRequired,
};