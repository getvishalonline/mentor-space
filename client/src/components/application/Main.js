import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router';
import 'rsuite/dist/styles/rsuite-default.css';
import { useDispatch } from 'react-redux';

import { attemptGetUser } from '../../store/thunks/user';

import { HomePage, RegisterPage, LoginPage, WelcomePage, LostPage } from '../pages/'
import { ProfilePage, MeetingRequestPage } from '../mentors/';
import { StartMeeting } from '../meetings/';

export default function Main({ location }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    let subscribed = true;

    dispatch(attemptGetUser())
    setLoading(false)
    return () => { subscribed = false; };
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  return !loading && (
    <div>
      <div className="main">
        <Switch>
          <Route exact path="/" component={WelcomePage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/home" component={HomePage} />
          <Route path="/meeting/:mentor_id/start" component={MeetingRequestPage} />
          <Route path="/mentor/:mentor_id" component={ProfilePage} />
          <Route path="/meeting" component={StartMeeting} />
          <Route path="*" component={LostPage} />
        </Switch>
      </div>
    </div>
  );
}

Main.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
};