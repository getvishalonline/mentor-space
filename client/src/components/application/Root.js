import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import '../../App.css';

import 'material-icons/iconfont/material-icons.css';
import 'rsuite/dist/styles/rsuite-default.css';

import Main from './Main';

export default function Root({ history, store }) {
  return (
    <Provider store={store}>
      
      <ConnectedRouter history={history}>
        <Main location={history.location}/>
      </ConnectedRouter>
    </Provider>
  );
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};