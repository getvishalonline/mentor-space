import React, { useRef, useState, useEffect } from 'react';

import * as Chime from 'amazon-chime-sdk-js';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import * as R from 'ramda';
import { getMentor, startMeeting, joinMeeting } from '../../api/mentors';

export default function StartMeeting({ location }) {
  const dispatch = useDispatch();
  const [meetingId, setMeetingId] = useState()
  const [meetingResponse, setMeetingResponse] = useState()
  const [attendeeResponse, setAttendeeResponse] = useState()
  let  meeting_id, attendee_id; 
  const [callCreated, setCallCreated] = useState(false)
  const videoElement = useRef()
  const { user } = useSelector(R.pick(['user']));

  useEffect(() => {
    const urlSearchParams = new URLSearchParams(location.search);
    const qparams = Object.fromEntries(urlSearchParams.entries());
    let attendee_id = qparams.attendee_id
  }, []);

  const startCall = () => { 
    const urlSearchParams = new URLSearchParams(location.search);
    const qparams = Object.fromEntries(urlSearchParams.entries());
    let attendee_id = qparams.attendee_id
    startMeeting({attendee_id: attendee_id})
      .then((data) => {
        setMeetingResponse(data.meetingResponse)
        console.log(meetingResponse);
        setCallCreated(true)
    });
  }

  const joinVideoCall = async () => {
    const urlSearchParams = new URLSearchParams(location.search);
    const qparams = Object.fromEntries(urlSearchParams.entries());
    attendee_id = qparams.attendee_id
    meeting_id = qparams.meeting_id ? qparams.meeting_id : meetingResponse.Meeting.MeetingId
    let attendee, meeting;
    await joinMeeting({attendee_id: attendee_id, meeting_id: meeting_id})
      .then((data) => {
        setAttendeeResponse(data.attendee)
        attendee = data.attendee
        console.log(data);
        meeting = data.meetingResponse
    });
    const logger = new Chime.ConsoleLogger('ChimeMeetingLogs', Chime.LogLevel.INFO);
    const deviceController = new Chime.DefaultDeviceController(logger);
    console.log(meeting);
    const configuration = new Chime.MeetingSessionConfiguration(meeting, attendee);
    const meetingSession = new Chime.DefaultMeetingSession(configuration, logger, deviceController);

    const observer = {
      audioVideoDidStart: () => {
        meetingSession.audioVideo.startLocalVideoTile();
      },
      videoTileDidUpdate: tileState => {
        meetingSession.audioVideo.bindVideoElement(tileState.tileId, videoElement.current);
      }
    }

    meetingSession.audioVideo.addObserver(observer);
    const firstVideoDeviceId = (await meetingSession.audioVideo.listVideoInputDevices())[0].deviceId;
    await meetingSession.audioVideo.chooseVideoInputDevice(firstVideoDeviceId);
    meetingSession.audioVideo.start();
  }

  return (
    <div className="App">
      <header className="App-header">
        <video ref={videoElement}></video>
        <p>Meeting Id: {meetingId}</p>
        <button onClick={joinVideoCall}> join call</button>
        <button onClick={startCall}>start call</button>
      </header>
    </div>
  );
}

StartMeeting.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired
};