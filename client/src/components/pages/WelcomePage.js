import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import * as R from 'ramda';

import { Container, Content, FlexboxGrid, Col } from 'rsuite';

export default function WelcomePage() {
  const dispatch = useDispatch();
  const { user } = useSelector(R.pick(['user']));

  useEffect(() => {
    if (!R.isEmpty(user)) {
      dispatch(push('/home'));
    }
  }, []);

  return (
    <div className="welcome-page page">
      <Container>
        <Content>
          <FlexboxGrid justify="center" align="middle" style={{height: '100vh'}}>
            <FlexboxGrid.Item componentClass={Col} colspan={24} xs={24}>
              <h3>Level Up with Mentorship</h3>
              <a href="/register" className="rs-btn rs-btn-primary">Sign Up</a>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
      </Container>
    </div>
  );
}