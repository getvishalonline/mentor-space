import HomePage from './HomePage';
import LoginPage from './LoginPage';
import RegisterPage from './RegisterPage';
import WelcomePage from './WelcomePage';
import LostPage from './LostPage';

export {
  HomePage,
  LoginPage,
  RegisterPage,
  WelcomePage,
  LostPage
};