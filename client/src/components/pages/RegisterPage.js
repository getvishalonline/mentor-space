import React, {useState, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import * as R from 'ramda';

import { Container, Schema, FlexboxGrid, Panel,
  Form, FormGroup, ControlLabel, FormControl, ButtonToolbar, Button,
  Col } from 'rsuite';

import { attemptRegister } from '../../store/thunks/auth';
import useKeyPress from '../../hooks/useKeyPress';

const { StringType } = Schema.Types;
const model = Schema.Model({
  username: StringType().isRequired('This field is required.'),
  email: StringType()
    .isEmail('Please enter a valid email address.')
    .isRequired('This field is required.'),
  about_me: StringType().isRequired('This field cannot be empty')
});

export default function RegisterPage() {
  const dispatch = useDispatch();
  let form = null;
  const { user } = useSelector(R.pick(['user']));
  const [formValue, setFormValue] = useState({
    username: '',
    email: '',
    about_me: '',
    password: ''
  })
  const [formError, setFormError] = useState({})

  const handleSubmit = () => {

    if (!form.check()) {
      console.error('Form Error');
      return;
    }
  
    dispatch(attemptRegister(formValue))
      .catch(R.identity);
  }

  useEffect(() => {
    if (!R.isEmpty(user)) {
      dispatch(push('/home'));
    }
  }, []);

  useKeyPress('Enter', handleSubmit);

  return (
    <div className="register-page page">
      <Container>
          <FlexboxGrid justify="center" align="middle" style={{height: '100vh'}}>
            <FlexboxGrid.Item componentClass={Col} colspan={12} xs={24} md={12}>
              <Panel header={<h3>Sign Up</h3>} bordered>
                <Form model={model}
                  ref={ (ref) => (form = ref)}
                  onCheck={formError => { setFormError({ formError }); }}
                  onChange={ formValue => setFormValue(formValue)}
                  formValue={formValue}
                fluid>
                  <FormGroup>
                    <ControlLabel>Username</ControlLabel>
                    <FormControl name="username" label="Username" />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>Email</ControlLabel>
                    <FormControl name="email" label="Email" />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>Password</ControlLabel>
                    <FormControl name="password" type="password" />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>About Me</ControlLabel>
                    <FormControl componentClass="textarea" rows={3} placeholder="Textarea" name="about_me"/>
                  </FormGroup>
                  <FormGroup>
                    <ButtonToolbar>
                      <Button appearance="primary" onClick={ handleSubmit }>Sign up</Button>
                    </ButtonToolbar>
                  </FormGroup>
                  <a href="/login">Already Registered? Click Here To Sign In</a>
                </Form>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
      </Container>
    </div>
  )
}

// class Register extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       formValue: {
//         name: '',
//         email: '',
//         about: '',
//         password: '',
//         verifyPassword: ''
//       },
//       formError: {}
//     };
//     this.handleSubmit = this.handleSubmit.bind(this);
//     // this.handleCheckEmail = this.handleCheckEmail.bind(this);
//   }
//   handleSubmit() {
//     const { formValue } = this.state;
//     if (!this.form.check()) {
//       console.error('Form Error');
//       return;
//     }
  
//     // dispatch(attemptRegister(formValue))
//     //   .catch(R.identity);
//   }

//   // handleCheckEmail() {
//   //   this.form.checkForField('email', checkResult => {
//   //     console.log(checkResult);
//   //   });
//   // }
//   render() {
//     const { formError, formValue } = this.state;

//     return (
//       <div className="register-page page">
//         <Container>
//             <FlexboxGrid justify="center" align="middle" style={{height: '100vh'}}>
//               <FlexboxGrid.Item componentClass={Col} colspan={12} xs={24} md={12}>
//                 <Panel header={<h3>Sign Up</h3>} bordered>
//                   <Form
//                     ref={ref => (form = ref)}
//                     onChange={formValue => {
//                       this.setState({ formValue });
//                     }}
//                     onCheck={formError => {
//                       this.setState({ formError });
//                     }}
//                     formValue={formValue}
//                     model={model}
//                     fluid
//                   >
                    
//                     <TextField name="name" label="Username" />

//                     <TextField name="email" label="Email" />
//                     <TextField name="about" label="About" componentClass="textarea" rows={3}  />
//                     <TextField name="password" label="Password" type="password" />

//                     <TextField name="verifyPassword" label="Verify password" type="password" />

//                     <ButtonToolbar>
//                       <Button appearance="primary" onClick={handleSubmit}>
//                         Submit
//                       </Button>

//                     </ButtonToolbar>
//                   </Form>
//                   </Panel>
//             </FlexboxGrid.Item>
//           </FlexboxGrid>
//         </Container>
//       </div>
//     );
//   }
// }