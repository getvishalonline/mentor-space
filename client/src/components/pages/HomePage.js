import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import * as R from 'ramda';
import { Container, FlexboxGrid, Grid, Col, FormGroup, Form, FormControl, Button, Schema, Panel} from 'rsuite';
import { searchMentors } from '../../store/thunks/mentors';
import { MentorCards } from '../molecules/mentorCards';
import Navigation from '../application/Navigation';
import useKeyPress from '../../hooks/useKeyPress';
const { StringType } = Schema.Types;
const model = Schema.Model({
  query: StringType().isRequired('This field is required.'),
});

export default function HomePage() {
  const dispatch = useDispatch();

  let form = null;
  const { user } = useSelector(R.pick(['user']));
  const [formValue, setFormValue] = useState({q: ''})
  const { mentors } = useSelector(R.pick(['mentors']));

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (R.isEmpty(user)) {
      dispatch(push('/login'));
    } else {
      dispatch(searchMentors({}))
      .catch(R.identity)
        .then(() => setLoading(false));
    }
  }, []);

  const searchMentor = () => {
    dispatch(searchMentors(formValue))
  }

  useKeyPress('Enter', searchMentor);
  
  return (
    <>
      <Navigation />
      <div className="home-page page">
        <Container>
          <FlexboxGrid justify="center" align="middle">
            <FlexboxGrid.Item componentClass={Col} colspan={12} xs={24}>
              <Panel header="Search" bordered>
                <Form 
                  model={model}
                  ref={ (ref) => (form = ref)}
                  onChange={ formValue => setFormValue(formValue)}
                  formValue={formValue}
                  layout="inline">
                  <FormGroup style={{position: 'relative'}}>
                    <FormControl name="q" placeholdee="Search Mentor" />
                    <Button appearance="default" onClick={ searchMentor } style={{background: 'transparent', position: 'absolute', right: '0'}}><span className="material-icons">search</span> </Button>
                  </FormGroup>
                </Form>
              </Panel>
            </FlexboxGrid.Item>
            <FlexboxGrid.Item componentClass={Grid} style={{marginTop: '20px'}}>
              { !loading ? <MentorCards key={'mentorList'} mentors={mentors} /> : '' }
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Container>
      </div>
    </>
  );
}