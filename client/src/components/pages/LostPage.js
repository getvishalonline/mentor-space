
import React from 'react';
import { Container, Content } from 'rsuite';

export default function LostPage() {
  return (
    <div className="lost-page page">
      <Container className="lost-section">
        <Content>
          <h2>404</h2>
          <p>
            The page you requested was not found.
          </p>
        </Content>
      </Container>
    </div>
  );
}