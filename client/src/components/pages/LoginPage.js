import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import * as R from 'ramda';

import { attemptLogin } from '../../store/thunks/auth';

import { Container, Content, Schema, FlexboxGrid, Panel,
  Form, FormGroup, ControlLabel, FormControl, ButtonToolbar, Button, Col } from 'rsuite';

const { StringType } = Schema.Types;
const model = Schema.Model({
  username: StringType().isRequired('This field is required.'),
  email: StringType()
    .isEmail('Please enter a valid email address.')
    .isRequired('This field is required.'),
  about_me: StringType().isRequired('This field cannot be empty')
});
  

export default function LoginPage() {
  const dispatch = useDispatch();
  let form = null;
  const { user } = useSelector(R.pick(['user']));
  const [formValue, setFormValue] = useState({
    username: '',
    password: '',
  })
  const [formError, setFormError] = useState({})

  useEffect(() => {
    if (!R.isEmpty(user)) {
      dispatch(push('/home'));
    }
  }, []);

  const handleSubmit = () => {
    dispatch(attemptLogin(formValue))
      .catch(R.identity);
  };

  return (
    <div className="show-fake-browser login-page">
      <Container>
        <Content>
          <FlexboxGrid justify="center" align="middle" style={{height: '100vh'}}>
            <FlexboxGrid.Item componentClass={Col} colspan={12} xs={24} md={6}>
              <Panel header={<h3>Sign In</h3>} bordered>
                <Form 
                model={model}
                ref={ (ref) => (form = ref)}
                onCheck={formError => { setFormError({ formError }); }}
                onChange={ formValue => setFormValue(formValue)}
                formValue={formValue}
                fluid>
                  <FormGroup>
                    <ControlLabel>Username</ControlLabel>
                    <FormControl name="username" />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>Password</ControlLabel>
                    <FormControl name="password" type="password" />
                  </FormGroup>
                  <FormGroup>
                    <ButtonToolbar>
                      <Button appearance="primary" onClick={ handleSubmit }>Sign in</Button>
                    </ButtonToolbar>
                  </FormGroup>
                  <a href="/register">Don't Have Account Yet? Click Here To Sign Up</a>
                </Form>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
      </Container>
    </div>
  );
}