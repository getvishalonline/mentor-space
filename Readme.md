## Mentor Space


## Setup

```bash
  $ npm install
  $ cd client && yarn
```

### Start local server

```bash
  $ npm run dev
```